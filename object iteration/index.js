const user = {
  name: "Ashish Panchal",
  address: {
    personal: {
      line1: "101",
      line2: "street Line",
      city: "NY",
      state: "WX",
      coordinates: {
        x: 35.12,
        y: -21.49,
      },
    },
    office: {
      city: "City",
      state: "WX",
      area: {
        landmark: "landmark",
      },
      coordinates: {
        x: 35.12,
        y: -21.49,
      },
    },
  },
  contact: {
    phone: {
      home: "xxx",
      office: "yyy",
    },
    other: {
      fax: "234",
    },
    email: {
      home: "xxx",
      office: "yyy",
    },
  },
};

// create a empty object for store a final output
let object = {};

// Create a function to for iteration object
function iterable(obj, name) {
  for (let key in obj) {
    // condition is show other get key to for loop is object or not
    if (typeof obj[key] == "object") {
      // this is new key and add  name and key
      iterable(obj[key], name + "_" + key);
    } else {
      // in store value in object
      object[name + "_" + key] = obj[key];
    }
  }
}
iterable(user, "user");
console.log(object);
