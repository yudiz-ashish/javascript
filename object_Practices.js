let user = {
  fname: "ashish",
  lname: "panchal",
  a: {
    b: {
      d: 5,
    },
  },
};

// only Object.Assign Are simple object not a nested object

// const user2 = { ...user };

const user2 = JSON.parse(JSON.stringify(user));
user2.age = 22;
user2.a.b.d = 10;
console.log(user);
console.log(user2);
