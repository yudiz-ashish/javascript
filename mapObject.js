const map = new Map();

const key1 = "str",
  key2 = {},
  key3 = function () {};

map.set(key1, "this is string");
map.set(key2, "this is blank object");
map.set(key3, "this is empty function");
console.log(map);
// getting value  from map

let value1 = map.get(key3);
console.log(value1);

//getting key and value  for of loop
for (const [key, value] of map) {
  console.log("key", key);
  console.log("value", value);
}
console.log("*".repeat(20));
//size of map
console.log(map.size);

//get Only Key same as value

console.log("*".repeat(20));

for (const key of map.keys()) {
  console.log("key is", key);
}

//forEach loop for map function

map.forEach((value, key) => {
  console.log("value is", value);
  console.log("key is ", key);
});

//converting map into array

let Myarray = Array.from(map);
console.log(Myarray);

// converting map key  into array
console.log("*".repeat(20));

let MymapKey = Array.from(map.keys());
console.log(MymapKey);
